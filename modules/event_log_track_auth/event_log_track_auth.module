<?php

/**
 * @file
 * Logs user authentication in the event_log_track module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Implements hook_event_log_track_handlers().
 */
function event_log_track_auth_event_log_track_handlers(): array {
  // User Authentication event log handler.
  $handlers = [];
  $handlers['authentication'] = [
    'title' => t('User authentication'),
    'form_ids' => ['user_login_form', 'user_pass'],
    'form_submit_callback' => 'event_log_track_auth_form_submit',
    'operations' => [
      'login',
      'logout',
      'request password',
      'fail',
    ],
  ];
  return $handlers;
}

/**
 * Event log callback for the user authentication event log.
 */
function event_log_track_auth_form_submit($form, $form_state, $form_id): ?array {
  $account = \Drupal::currentUser();
  $log = NULL;
  switch ($form_id) {
    case 'user_login_form':
      $uid = $account->id();
      // Don't log an anonymous user logging in. Can happen with 2FA.
      if ($uid === 0) {
        break;
      }
      // At this point, new session hasn't started yet.
      $session_count = _event_log_track_session_count($uid) + 1;
      $log = [
        'operation' => 'login',
        'description' => t('The user %user (uid %uid) has logged in successfully, current number of session counts %session_count', [
          '%user' => $form_state->getValue('name'),
          '%uid' => $uid,
          '%session_count' => $session_count,
        ]),
        'uid' => $uid,
        'ref_numeric' => $uid,
        'ref_char' => $account->getAccountName(),
      ];
      break;

    case 'user_pass':
      $account = $form_state->getValue('account');
      if (isset($account) && $account instanceof AccountInterface && $account->id() != 0) {
        $uid = $account->id();
      }
      else {
        return NULL;
      }

      $log = [
        'operation' => 'request password',
        'description' => t('The user %user (uid %uid) has requested resetting the password', [
          '%user' => $form_state->getValue('name'),
          '%uid' => $uid,
        ]),
        'ref_numeric' => $uid,
        'ref_char' => $form_state->getValue('name'),
      ];
      break;
  }
  return $log;
}

/**
 * Implements hook_user_logout().
 */
function event_log_track_auth_user_logout($account): void {
  $uid = $account->id();
  $session_left = _event_log_track_session_count($uid) - 1;

  $log = [
    'type' => 'authentication',
    'operation' => 'logout',
    'description' => t('The user %user (uid %uid) has logged out successfully, current number of session counts %session_count', [
      '%user' => $account->getAccount()->name,
      '%uid' => $uid,
      '%session_count' => $session_left,
    ]),
    'uid' => $uid,
    'ref_numeric' => $uid,
    'ref_char' => $account->getAccountName(),
  ];
  event_log_track_insert($log);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function event_log_track_auth_form_user_login_form_alter(&$form, FormStateInterface $form_state, $form_id): void {
  $form['#validate'][] = 'event_log_track_auth_user_login_validate';
}

/**
 * Implements hook_form_FORM_ID_validate().
 */
function event_log_track_auth_user_login_validate($form, $form_state): void {
  // Check for errors and log them.
  $errors = $form_state->getErrors();
  if (!empty($errors)) {
    $log = [
      'type' => 'authentication',
      'operation' => 'fail',
      'description' => 'Unrecognized username or password',
      'ref_char' => $form_state->getValue('name'),
    ];
    event_log_track_insert($log);
  }
}
