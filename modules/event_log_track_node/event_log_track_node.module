<?php

/**
 * @file
 * Logs node CUD commands in the event_log_track module.
 */

/**
 * Implements hook_event_log_track_handlers().
 */
function event_log_track_node_event_log_track_handlers(): array {
  // Node event log handler.
  $handlers = [];
  $handlers['node'] = [
    'title' => t('Node'),
    'operations' => [
      'insert',
      'update',
      'delete',
    ],
  ];
  return $handlers;
}

/**
 * Implements hook_node_insert().
 */
function event_log_track_node_insert($node): void {
  $log = [
    'type' => 'node',
    'operation' => 'insert',
    'description' => t('%type: %title, %status', [
      '%type' => $node->getType(),
      '%title' => $node->getTitle(),
      '%status' => ($node->isPublished()) ? 'Published' : 'Unpublished',
    ]),
    'ref_numeric' => $node->id(),
    'ref_char' => $node->getTitle(),
  ];
  event_log_track_insert($log);
}

/**
 * Implements hook_node_update().
 */
function event_log_track_node_update($node): void {
  $log = [
    'type' => 'node',
    'operation' => 'update',
    'description' => t('%type: %title, %status', [
      '%type' => $node->getType(),
      '%title' => $node->getTitle(),
      '%status' => ($node->isPublished()) ? 'Published' : 'Unpublished',
    ]),
    'ref_numeric' => $node->id(),
    'ref_char' => $node->getTitle(),
  ];
  event_log_track_insert($log);
}

/**
 * Implements hook_node_delete().
 */
function event_log_track_node_delete($node): void {
  $log = [
    'type' => 'node',
    'operation' => 'delete',
    'description' => t('%type: %title', [
      '%type' => $node->getType(),
      '%title' => $node->getTitle(),
    ]),
    'ref_numeric' => $node->id(),
    'ref_char' => $node->getTitle(),
  ];
  event_log_track_insert($log);
}

/**
 * Implements hook_views_data().
 */
function event_log_track_node_views_data(): array {
  $data['event_log_track']['elt_node_join'] = [
    'title' => t('Node ID'),
    'help' => t('Node ID.'),
    'relationship' => [
      'base' => 'node_field_data',
      'base field' => 'nid',
      'id' => 'standard',
      'field' => 'ref_numeric',
      'label' => t('Node ID'),
      'extra' => "event_log_track.type = 'node'",
    ],
  ];

  return $data;
}
